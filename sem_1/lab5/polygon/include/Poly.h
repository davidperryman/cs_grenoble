#include <iostream>
#include <cmath>
#include <vector>
using namespace std;

class Poly: public Function {
	private:
		vector<double> coeff;	
	public:
		Poly(int order, double* k): Function("Polynomial") {
			for (int i=0;i<order;i++) coeff.push_back(k[i]);
		
		}
		double eval(double x0) {
			double total = 0;
			for (int i=0;i<coeff.size();i++)
				total += coeff[i] * pow(x0, i);	
			return total;	
		}
		void print(ostream &o) {
			Function::print(o);
			o<<"f(x) = ";	
			for (int i=0;i<coeff.size();i++)
				o<<coeff[i]<<"*x^"<<i<<" + ";
			o<<endl;
		}
		Poly* derivative() {
			double *k1 = new double[coeff.size()-1];
			for (int i=1;i<coeff.size();i++)
				k1[i-1] = coeff[i]*i;	
			return new Poly(coeff.size()-1, k1);
		}
};
	
