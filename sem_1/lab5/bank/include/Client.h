#include <iostream>
#include <vector>
#include <string>
#define IRATEBLOCKED 4
#define IRATEUNBLOCKED 2

using namespace std;
typedef unsigned int uint;

// clients have a name, id, and 2 accounts (with all the functionality of those accounts)
class Client {
	private:
		string name;
		uint id, max;
		vector<Account*> accts;
	public:
		Client(string n, int m, uint i) {max=m; id=i; name=n;}
		Client(const Client& c): Client(c.name, c.max, c.id) {}	
		~Client() {while (!accts.empty()) {delete accts.back(); accts.pop_back();};}
		uint get_id() {return id;}	
		bool check() {return (accts.size()<max) ? false : true;}
		int debit (double a, int k) {if (check()) return accts[k]->debit(a); return -3;}
		int credit(double a, int k) {if (check()) return accts[k]->credit(a); return -3;}
		void print(ostream &);
		void createAccount();
};
void Client::createAccount() {
	if (check()) return;	
	bool type, blocked;
	cout<<"Creation of accounts for "<<name<<":\nSavings(0) or Current(1) 1/0?\n";
	cin>>type;	
	if (!type){
		cout<<"Blocked(1) or Unblocked(0) 1/0?\n";
		cin>>blocked;
		accts.push_back(new SavingsAccount(blocked));
	}
	else accts.push_back(new CurrentAccount());
}
// header only because it's cleaner
// fairly obvious implementation tbh 
void Client::print(ostream &o) {
	o<<"client: "<<name<<endl;
	o<<"id: "<<id<<endl;
	o<<"max: "<<max<<endl;
	for (int i=0; i<accts.size(); i++)
		operator<<(o, *(accts[i]));
}

