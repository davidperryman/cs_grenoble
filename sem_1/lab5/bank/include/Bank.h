#include<map>
#include <iostream>
using namespace std;
typedef unsigned int uint;

// bank is a collection of client* identified by their id. you can add clients or print them out
class Bank {
	private:
		map<uint, Client*> m;
	public:
		void insertClient(Client *c) {m.insert(pair<uint,Client*>(c->get_id(),c));}
		void print(ostream &o) {o<<"clients...\n"; 
			for(map<uint,Client*>::iterator i=m.begin();i!=m.end();++i) 
				i->second->print(o);}
};
