#include <iostream>
using namespace std;
typedef unsigned int uint;

// account has an id, balance, print method, ways to +- the balance, and getter for id
//  * debit is account dependent
class Account{
	private:
		uint id;
	protected:
		double balance;
		void print(ostream &o) const {
			o<<"account id: "<<id<<"\n\tbalance: "<<balance<<'\n';}

	public:
		Account() {balance=0;id=rand();}	
		uint get_id() {return id;}	
		int credit(double x) {balance+=x; return balance;}
		friend std::ostream &operator<<(ostream &o, const Account &a) {
			a.print(o); return o;}
		virtual int debit(double x) = 0;
};
