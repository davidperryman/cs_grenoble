#include <time.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "date.h"

class Trip {
	private:
		Date *start, *end;	
		float price;
	public:
		Trip(int d1, int m1, int y1, int d2, int m2, int y2, float p) {
			*start = Date(d1, m1, y1);
			*end = Date(d2, m2, y2);
			price = p;
		}
		Trip(Date* s, Date* e, float p) {
			start = s;
			end = e;
	 		price = p;
		}
		void print_trip() {
			printf("Start: ");
			start->print_date();
			printf("End: ");
			end->print_date();
			printf("Cost = %f\n", this->price_per_day());	
		}
		float price_per_day() {return price / end->difference(start);}
};
