// Includes
#include <typeinfo>
#include "Array_of_PPoint.h"
#include "PPoint.h"

using namespace std;

// shortcut
typedef unsigned int uint;

// PPoint
//  * hold 2 ints and have setters, printing, and constructors/destrcutors
// class function defs
PPoint::PPoint(int a, int b) {
	this->x = new int;
	this->y = new int;
	*x = a;
	*y = b;
	printf("in PPoint constructor, I am of type %s\n", typeid(*this).name());
}
PPoint::PPoint(const PPoint &p) {
	this->x = new int;
	this->y = new int;	
	*(this->x) = *(p.x);
	*(this->y) = *(p.y);
	printf("in PPoint constructor, I am of type %s\n", typeid(*this).name());
}
PPoint::~PPoint() {
	delete this->x;
	delete this->y;
}
void PPoint::add(PPoint *p) {
	int a, b;
	a = *(this->x) + *(p->x);
	b = *(this->y) + *(p->y);
	if (a<0 || a>20) throw "new x value out of bounds.";
	if (b<0 || b>20) throw "new y value out of bounds.";
	*(this->x) = a;
	*(this->y) = b;
}
void PPoint::set_coords(int a, int b) {
	*(this->x) = a;
	*(this->y) = b;

}
void PPoint::print() const {
	printf("x: %d, y: %d\n", *(this->x), *(this->y));
}
