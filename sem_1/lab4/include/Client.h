#include <iostream>
#include <string>
#define IRATEBLOCKED 4
#define IRATEUNBLOCKED 2

using namespace std;
typedef unsigned int uint;

// clients have a name, id, and 2 accounts (with all the functionality of those accounts)
class Client {
	private:
		string name;
		uint id;
		CurrentAccount* c;
		SavingsAccount* s;
	public:
		Client(string n) {c=NULL; s=NULL; id=rand();name=n;}
		~Client() {if (c!=NULL) delete c; if (s!=NULL) delete s;}	
		uint get_id() {return id;}	
		// I use check to create empty accounts if necessary	
		void check_c() {if (c==NULL) c = new CurrentAccount();}	
		void check_s() {if (s==NULL) s = new SavingsAccount(false);}	
		// credit/debit savings/current just checks and calls appropriate func	
		void credit_savings(double a) {check_s(); s->credit(a);}	
		void credit_current(double a) {check_c(); c->credit(a);}	
		int debit_savings(double a) {check_s(); return s->debit(a);}	
		int debit_current(double a) {check_c(); return c->debit(a);}	
		void print(ostream &);
};
// header only because it's cleaner
// fairly obvious implementation tbh 
void Client::print(ostream &o) {
	o<<"client: "<<name<<endl;
	o<<"id: "<<id<<endl;
	o<<"checking account: \n";
	check_c();
	c->print(o);
	o<<"savings account: \n";
	check_s();
	s->print(o);
}

