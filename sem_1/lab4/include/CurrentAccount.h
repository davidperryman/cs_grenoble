#include <iostream>
using namespace std;
typedef unsigned int uint;

// current accounts have a pretty friendly debit function and a specific printer
class CurrentAccount: public Account {
	public:
		// should have a special debit function	
		int debit(double x);
		// add the type to the output of Account	
		void print(ostream &o) {Account::print(o);o<<"\ttype: Current Account\n";}
};

int CurrentAccount::debit(double x) {
	if (balance==0) return -1;	
	else if ((balance-x)<0) {
		double received=balance-x;
		balance=0.;
		return -2;
	}
	else {
		double received=x;
		balance -=x;
		return 1;
	}
}

