#include <iostream>
using namespace std;
typedef unsigned int uint;

// account has an id, balance, print method, ways to +- the balance, and getter for id
//  * debit is account dependent
class Account{
	private:
		uint id;
	protected:
		double balance;
	public:
		Account() {balance=0;id=rand();}	
		uint get_id() {return id;}	
		void credit(double x) {balance+=x;}
		int debit(double x);
		void print(ostream &o) {o<<"account id: "<<id<<"\n\tbalance: "<<balance<<'\n';}
};
