#include <iostream>
#define IRATEBLOCKED 4
#define IRATEUNBLOCKED 2

using namespace std;
typedef unsigned int uint;

// savings accounts are just regular accounts with:
//  * conditional restriction on withdrawal (blocked)
//  * interest
//  * a strict withdrawal system
class SavingsAccount: public Account {
	private:
		// 1 for blocked		
		bool blocked;
	public:
		SavingsAccount(bool is_blocked=false){blocked=is_blocked;}	
		int debit(double x);
		double my_rate(){if (blocked) return IRATEBLOCKED/100.; return IRATEUNBLOCKED/100.;}	
		void add_interest() {credit(balance*my_rate());}
		void print(ostream &o) {
			Account::print(o);
			o<<"\ttype: Savings Account\n\tblocked: "<<blocked<<"\n";}
};
// header only soln bc this is the only function outside
int SavingsAccount::debit(double x) {
	if (blocked) return -1;	
	if ((balance-x)<0) {
		return -1;
	}
	else {
		balance -=x;
		return 1;
	}
}

